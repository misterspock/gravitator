// test/testBody.js

import { expect } from "chai";
import Body from "../src/Body";
import conf from "../src/conf";

describe('Body object', function() {

    it('should return valid instance of self when "createNew" static method is called', function() {
        var startCoords = {
                mouseX: 0,
                mouseY: 0
            },
            endCoords = {
                mouseX: 10,
                mouseY: 10
            },
            body = Body.createNew(startCoords, endCoords, 1, ''),

            expectedSpeedX = (endCoords.mouseX - startCoords.mouseX) / conf.startSpeedDampenFactor,
            expectedSpeedY = (endCoords.mouseY - startCoords.mouseY) / conf.startSpeedDampenFactor;

        expect(body.x).to.be.equal(0);
        expect(body.y).to.be.equal(0);

        expect(body.radius).to.be.equal(conf.bodyDefaults.maxRadius);
        expect(body.mass).to.be.equal(conf.bodyDefaults.maxMass);

        expect(body.velX).to.be.equal(expectedSpeedX);
        expect(body.velY).to.be.equal(expectedSpeedY);
    });

    it('should move accordingly to it\'s speed and passed time with "move" method', function() {
        const body = new Body(0, 0, 10, 10, 0, 0, '');

        body.velX = 10;
        body.velY = 10;

        body.move(1);

        expect(body.x).to.be.equal(10);
        expect(body.y).to.be.equal(10);
    });

    it('should return valid acceleration vector object on "getAccVector" method call', function() {
        const bodyOne = new Body(0, 0, 10, 20, 0, 0, '');
        const bodyTwo = new Body(100, 100, 10, 20, 0, 0, '');
        const vector  = bodyOne.getAccVector(bodyTwo);

        expect(vector.acceleration.toFixed(6)).to.be.equal('0.006674');
        expect(vector.relAngle.toFixed(4)).to.be.equal('0.7854');
    });

    it('should return valid axial accelerations on "getAxialAcc" method call', function() {
        const bodyOne  = new Body(0, 0, 10, 20, 0, 0, '');
        const bodyTwo  = new Body(100, 100, 10, 20, 0, 0, '');
        const axialAcc = bodyOne.getAxialAcc(bodyTwo);

        expect(axialAcc.accX.toFixed(6)).to.be.equal('0.004719');
        expect(axialAcc.accY.toFixed(6)).to.be.equal('0.004719');
    });

    it('should jump coordinates when Body is out of bounds of view field, modelling thoroidal space', function() {
        const body      = new Body(0, 0, 0, 0, 0, 0, '');
        const bounds    = conf.viewportBounds;
        const genCoords = [
                {
                    x: conf.canvasWidth / 2,
                    y: bounds.up - 10,
                },
                {
                    x: conf.canvasWidth / 2,
                    y: bounds.down + 10,
                },
                {
                    x: bounds.left - 10,
                    y: conf.canvasHeight / 2,
                },
                {
                    x: bounds.right + 10,
                    y: conf.canvasHeight / 2,
                }
            ];

        genCoords.forEach(coords => {
            body.x = coords.x;
            body.y = coords.y;

            body.checkBounds();

            expect(body.x).to.be.within(bounds.left, bounds.right);
            expect(body.y).to.be.within(bounds.up, bounds.down);
        });
    });
});
