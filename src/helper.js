/**
 * Helper functions
 */

/**
 * Draws a circle in given context.
 * @param {Context} context A canvas context to draw within
 * @param {Number}  x       X coordinate of th circle to draw
 * @param {Number}  y       Y coordinate of th circle to draw
 * @param {Number}  radius  Radius size of a circle
 * @param {String}  color   A HEX representation of circle color
 */
function circle(context, x, y, radius, color) {
    context.fillStyle = color;
    
    context.beginPath();
    context.arc(x, y, radius, (Math.PI * 2), false);
    context.closePath();
    context.fill();
}

function mouseCoords(ev, canvas, window) {
    const offsetX = canvas.offsetLeft - window.pageXOffset, // Account window scroll here!
          offsetY = canvas.offsetTop - window.pageYOffset;  //

    return {
        mouseX: parseInt(ev.clientX - offsetX, 10),
        mouseY: parseInt(ev.clientY - offsetY, 10)
    };
}

/**
 * Returns a random string ID.
 * @returns {String}
 */
function randomId() {
    return Math.random().toString(16).substr(2);
}

export { circle, mouseCoords, randomId };
