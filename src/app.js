import Body from "./Body";
import World from "./World";
import { mouseCoords, circle } from "./helper";
import conf from "./conf";

const canvas     = document.getElementById('main-viewport');
const colorInp   = document.getElementById('color');
const scaleInp   = document.getElementById('scale');
const context    = canvas.getContext('2d');
const world      = new World();

let startCoords = {},
    oldTime     = Date.now();

// Setting up canvas dimensions
canvas.width = conf.canvasWidth;
canvas.height = conf.canvasHeight;

// TEST BLOCK
const bodies = [
    Body.createNew({ mouseX: 400, mouseY: 300 }, { mouseX: 400, mouseY: 300 }, 1,   '#ffffff'),
    Body.createNew({ mouseX: 300, mouseY: 300 }, { mouseX: 300, mouseY:   0 }, 0.2, '#ffffff'),
    Body.createNew({ mouseX: 400, mouseY: 200 }, { mouseX: 700, mouseY: 200 }, 0.2, '#ffffff'),
    Body.createNew({ mouseX: 500, mouseY: 300 }, { mouseX: 500, mouseY: 600 }, 0.2, '#ffffff'),
    Body.createNew({ mouseX: 400, mouseY: 400 }, { mouseX: 100, mouseY: 400 }, 0.2, '#ffffff')
];

bodies.map(body => world.addBody(body));
// END TEST BLOCK

// Setup default color
colorInp.value = '#ffffff';

function render() {
    clearField();

    const newTime = Date.now();
    const dt      = (newTime - oldTime) / 1000 * conf.timeFactor; // Milliseconds to seconds

    world.tick(dt);
    world.getState()
        .forEach(body => circle(context, body.x, body.y, body.radius, body.color));

    oldTime = newTime;

    window.requestAnimationFrame(render);
}

function clearField() {
    context.fillStyle = 'rgba(0, 0, 0, 0.1)';
    context.fillRect(0, 0, canvas.width, canvas.height);
}

canvas.onmousedown = (ev) => {
    try {
        startCoords = mouseCoords(ev, canvas, window);
    }
    catch (err) {
        console.warn('On Mousedown handler error');
        console.error(err.message);
    }
};

canvas.onmouseup = (ev) => {
    try {
        const endCoords = mouseCoords(ev, canvas, window),
              scale     = parseFloat(scaleInp.value),
              newBody   = Body.createNew(startCoords, endCoords, scale, colorInp.value);

        world.addBody(newBody);
    }
    catch (err) {
        console.warn('No Body was added to World due to error.');
        console.error(err);
    }
};

// Use 'Esc' to clear World
window.onkeyup = (ev) => {
    if (ev.keyCode === 27) {
        world.resetState();
    }
};

document.onreadystatechange = () => {
    if (document.readyState == "interactive") {
        render();
    }
}
