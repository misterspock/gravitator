import conf from "./conf";

/**
 * A class that describes a body and calculations related to it.
 */
class Body {

    constructor(x, y, radius, mass, velX, velY, color) {
        this.id     = "";
        this.x      = x;
        this.y      = y;
        this.radius = radius;
        this.mass   = mass;
        this.velX   = velX;
        this.velY   = velY;
        this.accX   = 0;
        this.accY   = 0;
        this.color  = color;
    }

    getAccVector(otherBody) {
        const minDist  = this.radius + otherBody.radius;
        const trueDist = Math.sqrt(Math.pow(otherBody.x - this.x, 2) + Math.pow(otherBody.y - this.y, 2));

        let relAngle     = 0,
            acceleration = 0;

        // Turn off any physics when bodies are closer that the sum of their radiuses
        if (minDist < trueDist) {
            relAngle     = Math.atan2((otherBody.y - this.y), (otherBody.x - this.x));
            acceleration = (conf.gravConst * otherBody.mass) / Math.pow(trueDist, 2);
        }

        return { acceleration, relAngle };
    }

    getAxialAcc(otherBody) {
        const { acceleration, relAngle } = this.getAccVector(otherBody);

        return {
            accX : acceleration * Math.cos(relAngle),
            accY : acceleration * Math.sin(relAngle)
        };
    }

    calculateAcceleration(otherBodies) {
        // Zero this stuff.
        this.accX = 0;
        this.accY = 0;

        (otherBodies || []).forEach(otherBody => {
            const { accX, accY } = this.getAxialAcc(otherBody);
            this.accX += accX;
            this.accY += accY;
        });
    }

    move(dt) {
        this.velX = this.velX + this.accX * dt;
        this.velY = this.velY + this.accY * dt;

        this.x = this.x + this.velX * dt;
        this.y = this.y + this.velY * dt;

        this.checkBounds();
    }

    checkBounds() {
        const bounds = conf.viewportBounds;

        if (this.x > bounds.right) {
            this.x = bounds.left;
        }
        else if (this.x < bounds.left) {
            this.x = bounds.right;
        }

        if (this.y > bounds.down) {
            this.y = bounds.up;
        }
        else if (this.y < bounds.up) {
            this.y = bounds.down;
        }
    }
}

Body.createNew = function(startCoords, endCoords, scale, color) {
    const radius = conf.bodyDefaults.maxRadius * scale;
    const mass   = conf.bodyDefaults.maxMass * Math.pow(scale, 2);

    const velX = (endCoords.mouseX - startCoords.mouseX) / conf.startSpeedDampenFactor;
    const velY = (endCoords.mouseY - startCoords.mouseY) / conf.startSpeedDampenFactor;

    return new Body(startCoords.mouseX, startCoords.mouseY, radius, mass, velX, velY, color);
};

export default Body;
