/**
 * Application configuration.
 */

const canvasWidth  = 800,
      canvasHeight = 600,

      startSpeedDampenFactor = 100.0,
      timeFactor             = 10,

      gravConst = parseFloat('6.674e−11'),

      maxRadius = 25,
      maxMass   = 250;

export default {
    canvasWidth,
    canvasHeight,

    startSpeedDampenFactor,
    timeFactor,

    gravConst,

    bodyDefaults: {
        maxRadius,
        maxMass
    },

    viewportBounds: {
        up    : -50,
        down  : canvasHeight + 50,
        left  : -50,
        right : canvasWidth + 50
    }
};
