import Body from "./Body";
import { randomId } from "./helper";

/**
 * CLass that describes a world and it's physics.
 */
class World {
    
    constructor() {
        this._bodies = [];
    }

    addBody(body) {
        if (!(body instanceof Body)) {
            throw new Error("No Body instance given to add to World.");
        }

        body.id = randomId();
        this._bodies.push(body);
    }

    resetState() {
        this._bodies = [];
    }

    tick(deltaTime) {
        this._bodies.forEach(body => {
            const otherBodies = this._bodies.filter(otherBody => otherBody.id !== body.id);
            body.calculateAcceleration(otherBodies);
        });

        this._bodies.forEach(body => body.move(deltaTime));
    }

    getState() {
        return this._bodies;
    }
}

export default World;
