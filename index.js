(function () {
  'use strict';

  function _classCallCheck(a, n) {
    if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function");
  }
  function _defineProperties(e, r) {
    for (var t = 0; t < r.length; t++) {
      var o = r[t];
      o.enumerable = o.enumerable || false, o.configurable = true, "value" in o && (o.writable = true), Object.defineProperty(e, _toPropertyKey(o.key), o);
    }
  }
  function _createClass(e, r, t) {
    return r && _defineProperties(e.prototype, r), Object.defineProperty(e, "prototype", {
      writable: false
    }), e;
  }
  function _toPrimitive(t, r) {
    if ("object" != typeof t || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (undefined !== e) {
      var i = e.call(t, r);
      if ("object" != typeof i) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (String )(t);
  }
  function _toPropertyKey(t) {
    var i = _toPrimitive(t, "string");
    return "symbol" == typeof i ? i : i + "";
  }

  /**
   * Application configuration.
   */

  var canvasWidth = 800,
    canvasHeight = 600,
    startSpeedDampenFactor = 100.0,
    timeFactor = 10,
    gravConst = parseFloat('6.674e−11'),
    maxRadius = 25,
    maxMass = 250;
  var conf = {
    canvasWidth: canvasWidth,
    canvasHeight: canvasHeight,
    startSpeedDampenFactor: startSpeedDampenFactor,
    timeFactor: timeFactor,
    gravConst: gravConst,
    bodyDefaults: {
      maxRadius: maxRadius,
      maxMass: maxMass
    },
    viewportBounds: {
      up: -50,
      down: canvasHeight + 50,
      left: -50,
      right: canvasWidth + 50
    }
  };

  /**
   * A class that describes a body and calculations related to it.
   */
  var Body = /*#__PURE__*/function () {
    function Body(x, y, radius, mass, velX, velY, color) {
      _classCallCheck(this, Body);
      this.id = "";
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.mass = mass;
      this.velX = velX;
      this.velY = velY;
      this.accX = 0;
      this.accY = 0;
      this.color = color;
    }
    return _createClass(Body, [{
      key: "getAccVector",
      value: function getAccVector(otherBody) {
        var minDist = this.radius + otherBody.radius;
        var trueDist = Math.sqrt(Math.pow(otherBody.x - this.x, 2) + Math.pow(otherBody.y - this.y, 2));
        var relAngle = 0,
          acceleration = 0;

        // Turn off any physics when bodies are closer that the sum of their radiuses
        if (minDist < trueDist) {
          relAngle = Math.atan2(otherBody.y - this.y, otherBody.x - this.x);
          acceleration = conf.gravConst * otherBody.mass / Math.pow(trueDist, 2);
        }
        return {
          acceleration: acceleration,
          relAngle: relAngle
        };
      }
    }, {
      key: "getAxialAcc",
      value: function getAxialAcc(otherBody) {
        var _this$getAccVector = this.getAccVector(otherBody),
          acceleration = _this$getAccVector.acceleration,
          relAngle = _this$getAccVector.relAngle;
        return {
          accX: acceleration * Math.cos(relAngle),
          accY: acceleration * Math.sin(relAngle)
        };
      }
    }, {
      key: "calculateAcceleration",
      value: function calculateAcceleration(otherBodies) {
        var _this = this;
        // Zero this stuff.
        this.accX = 0;
        this.accY = 0;
        (otherBodies || []).forEach(function (otherBody) {
          var _this$getAxialAcc = _this.getAxialAcc(otherBody),
            accX = _this$getAxialAcc.accX,
            accY = _this$getAxialAcc.accY;
          _this.accX += accX;
          _this.accY += accY;
        });
      }
    }, {
      key: "move",
      value: function move(dt) {
        this.velX = this.velX + this.accX * dt;
        this.velY = this.velY + this.accY * dt;
        this.x = this.x + this.velX * dt;
        this.y = this.y + this.velY * dt;
        this.checkBounds();
      }
    }, {
      key: "checkBounds",
      value: function checkBounds() {
        var bounds = conf.viewportBounds;
        if (this.x > bounds.right) {
          this.x = bounds.left;
        } else if (this.x < bounds.left) {
          this.x = bounds.right;
        }
        if (this.y > bounds.down) {
          this.y = bounds.up;
        } else if (this.y < bounds.up) {
          this.y = bounds.down;
        }
      }
    }]);
  }();
  Body.createNew = function (startCoords, endCoords, scale, color) {
    var radius = conf.bodyDefaults.maxRadius * scale;
    var mass = conf.bodyDefaults.maxMass * Math.pow(scale, 2);
    var velX = (endCoords.mouseX - startCoords.mouseX) / conf.startSpeedDampenFactor;
    var velY = (endCoords.mouseY - startCoords.mouseY) / conf.startSpeedDampenFactor;
    return new Body(startCoords.mouseX, startCoords.mouseY, radius, mass, velX, velY, color);
  };

  /**
   * Helper functions
   */

  /**
   * Draws a circle in given context.
   * @param {Context} context A canvas context to draw within
   * @param {Number}  x       X coordinate of th circle to draw
   * @param {Number}  y       Y coordinate of th circle to draw
   * @param {Number}  radius  Radius size of a circle
   * @param {String}  color   A HEX representation of circle color
   */
  function circle(context, x, y, radius, color) {
    context.fillStyle = color;
    context.beginPath();
    context.arc(x, y, radius, Math.PI * 2, false);
    context.closePath();
    context.fill();
  }
  function mouseCoords(ev, canvas, window) {
    var offsetX = canvas.offsetLeft - window.pageXOffset,
      // Account window scroll here!
      offsetY = canvas.offsetTop - window.pageYOffset; //

    return {
      mouseX: parseInt(ev.clientX - offsetX, 10),
      mouseY: parseInt(ev.clientY - offsetY, 10)
    };
  }

  /**
   * Returns a random string ID.
   * @returns {String}
   */
  function randomId() {
    return Math.random().toString(16).substr(2);
  }

  /**
   * CLass that describes a world and it's physics.
   */
  var World = /*#__PURE__*/function () {
    function World() {
      _classCallCheck(this, World);
      this._bodies = [];
    }
    return _createClass(World, [{
      key: "addBody",
      value: function addBody(body) {
        if (!(body instanceof Body)) {
          throw new Error("No Body instance given to add to World.");
        }
        body.id = randomId();
        this._bodies.push(body);
      }
    }, {
      key: "resetState",
      value: function resetState() {
        this._bodies = [];
      }
    }, {
      key: "tick",
      value: function tick(deltaTime) {
        var _this = this;
        this._bodies.forEach(function (body) {
          var otherBodies = _this._bodies.filter(function (otherBody) {
            return otherBody.id !== body.id;
          });
          body.calculateAcceleration(otherBodies);
        });
        this._bodies.forEach(function (body) {
          return body.move(deltaTime);
        });
      }
    }, {
      key: "getState",
      value: function getState() {
        return this._bodies;
      }
    }]);
  }();

  var canvas = document.getElementById('main-viewport');
  var colorInp = document.getElementById('color');
  var scaleInp = document.getElementById('scale');
  var context = canvas.getContext('2d');
  var world = new World();
  var startCoords = {},
    oldTime = Date.now();

  // Setting up canvas dimensions
  canvas.width = conf.canvasWidth;
  canvas.height = conf.canvasHeight;

  // TEST BLOCK
  var bodies = [Body.createNew({
    mouseX: 400,
    mouseY: 300
  }, {
    mouseX: 400,
    mouseY: 300
  }, 1, '#ffffff'), Body.createNew({
    mouseX: 300,
    mouseY: 300
  }, {
    mouseX: 300,
    mouseY: 0
  }, 0.2, '#ffffff'), Body.createNew({
    mouseX: 400,
    mouseY: 200
  }, {
    mouseX: 700,
    mouseY: 200
  }, 0.2, '#ffffff'), Body.createNew({
    mouseX: 500,
    mouseY: 300
  }, {
    mouseX: 500,
    mouseY: 600
  }, 0.2, '#ffffff'), Body.createNew({
    mouseX: 400,
    mouseY: 400
  }, {
    mouseX: 100,
    mouseY: 400
  }, 0.2, '#ffffff')];
  bodies.map(function (body) {
    return world.addBody(body);
  });
  // END TEST BLOCK

  // Setup default color
  colorInp.value = '#ffffff';
  function render() {
    clearField();
    var newTime = Date.now();
    var dt = (newTime - oldTime) / 1000 * conf.timeFactor; // Milliseconds to seconds

    world.tick(dt);
    world.getState().forEach(function (body) {
      return circle(context, body.x, body.y, body.radius, body.color);
    });
    oldTime = newTime;
    window.requestAnimationFrame(render);
  }
  function clearField() {
    context.fillStyle = 'rgba(0, 0, 0, 0.1)';
    context.fillRect(0, 0, canvas.width, canvas.height);
  }
  canvas.onmousedown = function (ev) {
    try {
      startCoords = mouseCoords(ev, canvas, window);
    } catch (err) {
      console.warn('On Mousedown handler error');
      console.error(err.message);
    }
  };
  canvas.onmouseup = function (ev) {
    try {
      var endCoords = mouseCoords(ev, canvas, window),
        scale = parseFloat(scaleInp.value),
        newBody = Body.createNew(startCoords, endCoords, scale, colorInp.value);
      world.addBody(newBody);
    } catch (err) {
      console.warn('No Body was added to World due to error.');
      console.error(err);
    }
  };

  // Use 'Esc' to clear World
  window.onkeyup = function (ev) {
    if (ev.keyCode === 27) {
      world.resetState();
    }
  };
  document.onreadystatechange = function () {
    if (document.readyState == "interactive") {
      render();
    }
  };

})();
