import resolve from "@rollup/plugin-node-resolve";
import babel from "@rollup/plugin-babel";

export default {
    input: "src/app.js",
    output: {
        file   : "index.js",
        format : "iife"
    },
    plugins: [
        resolve(),
        babel({
            babelHelpers: 'bundled',
            exclude: "node_modules/**"
        })
    ]
};
