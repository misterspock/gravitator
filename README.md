### Gravitator

Simple project to implement interactive gravity simulator (N-body problem solver)
using Newtonean physics in HTML5 Canvas.

To run simply open `index.html`.

#### Development

To install:

    - clone repository
    - cd in project's folder
    - run 'npm install'

Build `index.js` with rollup:

    - run 'npm run build'

Run Mocha/Chai based unit tests:

    - run 'npm test'

Enjoy! :)
